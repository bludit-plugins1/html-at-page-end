Html At Page End
================================
Html At Page End Plugin for the [**Bludit CMS**](https://www.bludit.com/)

This plugin adds HTML under every page



Customizing
-------

The output is within a div element with the class "html-at-page-end" which you can use to add some styling.

License
-------
This is open source software licensed under the [MIT license](https://tldrlegal.com/license/mit-license).
