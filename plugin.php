<?php

class pluginHtmlEndPage extends Plugin {

    public function init()
    {
        $this->dbFields = array(
            'html' => '',
        );
    }

    public function form()
    {
        global $L;

        $html  = '<div class="alert alert-primary" role="alert">';
        $html .= $this->description();
        $html .= '</div>';

        $html .= '<div>';
        $html .= '<label>'.$L->get('Html').'</label>';
        $html .= '<textarea style="width:80%;height:300px" name="html">'.$this->getValue('html').'</textarea>';
        $html .= '</div>';

        return $html;
    }

    public function pageEnd()
    {
        global $page;
        global $url;


        $html = '';
        if($url->whereAmI() == 'page' && $page->type() != 'static' && !$url->notFound()) {
            $html .= '<div class="html-at-page-end">';
            $html .= $this->getValue('html',false);
            $html .= '</div>';
        }
        return $html;
    }


}
